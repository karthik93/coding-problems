#include<iostream>
#include<math.h>
using namespace std;

int main(){
    std::string s;
    cin >> s;
    bool isEven = ( (s.length()%2)== 0);
    bool isPal = true;
    if(isEven){
        for(int i=0; i <ceil((s.length()-1)/2); i++ ){
            if(!(s[i] == s[((s.length()-1)-i)])){
                isPal = false;
            }
        }
    }
    if(isPal)
        cout << "true" <<endl;
    else
        cout << "false" <<endl;

    return 0;
}
