import java.util.Scanner;
class capslock {
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        String line = in.nextLine();
        boolean case1 = false;
        for(int i =0; i< line.length(); i++){
        char c = line.charAt(i);
		if(c=='a'||c=='A'){
			case1 = !case1;
		}
		if(case1){
			System.out.print(Character.toUpperCase(c));
		}else{
			System.out.print(Character.toLowerCase(c));
		}
	}
    }
}
